package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import Graphs.Main;
	
	import starling.core.Starling;
	
	public class Graphs extends Sprite
	{
		public function Graphs()
		{
			super();
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e : Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			var timer : Timer = new Timer(100, 1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE,initStarling);
			timer.start();
		}
		
		private function initStarling(e : TimerEvent) : void
		{
			e.currentTarget.removeEventListener(TimerEvent.TIMER_COMPLETE,initStarling);
			
//			Starling.multitouchEnabled = true;
			
			var viewPort:Rectangle = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			var starling:Starling = new Starling(Main, this.stage, viewPort, null, Context3DRenderMode.AUTO);
//			starling.simulateMultitouch = true;
			starling.antiAliasing = 1;
			starling.start();
		}
		
	}
}