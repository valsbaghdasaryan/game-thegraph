package
{
	import starling.animation.Transitions;

	public class Const
	{
		//Texture names
		public static const TEXTURE_DOT : String = "white_ball";
		public static const TEXTURE_LEADERBOARD : String = "leaderboard";
		public static const TEXTURE_HOME: String = "home";
		public static const TEXTURE_PLAY: String = "play";
		public static const TEXTURE_RATE: String = "rate";
		public static const TEXTURE_RETRY: String = "retry";
		public static const TEXTURE_SOUND: String = "sound";
		public static const TEXTURE_TITLE: String = "GRAPHS";
		public static const TEXTURE_TUBE: String = "whiteTube";
		public static const TEXTURE_CIRCLE_WHITE: String = "icon-bg";
		
		// GAME COLORS
		public static const COLOR_BG : int = 0xffffff;
		public static const COLOR_GREY_LIGHT : int = 0xcccccc;
		public static const COLOR_GREY_DARK : int = 0x888888;
		
		//GAME MODES
		public static const MODE_GAME : String = "MODE_GAME";
		public static const MODE_MENU : String = "MODE_MENU";
		
		// transitions
		public static const TRANSITION_TIME : Number = 0.5;
		public static const TRANSITION_NAME : String = Transitions.EASE_IN_OUT;
		public static const DIRECTION_LEFT : String = "left";
		public static const DIRECTION_RIGHT: String = "right";
		public static const DIRECTION_UP: String = "up";
		public static const DIRECTION_DOWN: String = "down";
		
		
		// game components
		public static const DOT_IS_FULL:String = "DOT_IS_FULL";
		public static const DOT_PUSHED:String = "DOT_PUSHED";
		public static const ENABLE_DOTS:String = "ENABLE_DOTS";
		public static const READY:String = "READY";
		public static const ALL_DOTS_READY:String = "ALL_DOTS_READY";
		public static const ALL_LINES_READY:String = "ALL_LINES_READY";
		public static const LINES_ARE_CREATED:String = "LINES_ARE_CREATED";
		public static const TOUCH_ENDED:String = "TOUCH_ENDED";
		
		// game 
		public static const GAME_OVERED:String = "GAME_OVERED";
		public static const RESET_LEVEL:String = "RESET_LEVEL";
		public static const LEVEL_COMPLETED:String = "LEVEL_COMPLETED";
		public static const WRONG_DOT:String = "WRONG_DOT";
		public static const NO_CONNECTED_DOTS:String = "NO_CONNECTED_DOTS";
		
		// events
		public static const LEVELS_LOADED:String = "LELVES_LOADED";
		
		// page controller
		public static const LEVEL_SELECTED:String = "LEVEL_SELECTED";
		public static const RESET:String = "RESET";
		public static const UNLOCKED_NUMBER_CHANGED:String = "UNLOCKED_NUMBER_CHANGED";
		
		
		//resolution
		public static var SCREEN_WIDTH : Number;
		public static var SCREEN_HEIGHT : Number;
		public static var SCREEN_SIZE : Number;
		public static var GAME_HEIGHT : Number;
		
		
		
		
		
	}
}