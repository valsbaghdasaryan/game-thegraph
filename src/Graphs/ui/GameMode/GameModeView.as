package Graphs.ui.GameMode
{
	import Graphs.Main;
	import Graphs.ui.GameMode.levels.Level;
	import Graphs.ui.GameMode.levels.LevelGenerator;
	import Graphs.ui.components.BaseMode;
	import Graphs.ui.components.ButtonSpecial;
	import Graphs.ui.components.IMode;
	import Graphs.utils.GameUtils;
	import Graphs.utils.LayoutUtils;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class GameModeView extends BaseMode implements IMode
	{
		private var _currentLevel:Level;
		private var _gameContainer:Sprite;
		private var _retryButton:ButtonSpecial;
		private var _retryYCoord:Number;
		private var _buttonsContainer:Sprite;
		private var _levelContainer:Sprite;
		private var _buttonSize:Number;
		
		public function GameModeView()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		protected function initialize(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			initBg();
			initButtons();
			
			_levelContainer = new Sprite;
			addChildAt(_levelContainer,1);
			
			LevelGenerator.$.addEventListener(Const.RESET_LEVEL, resetLevelHandler);
			LevelGenerator.$.addEventListener(Const.GAME_OVERED, gameOveredHandler);
			LevelGenerator.$.addEventListener(Const.LEVEL_COMPLETED, levelCompletedHandler);
			
		}
		
		private function initButtons():void
		{
			var padding : Number = Const.SCREEN_SIZE * .03;
			_buttonSize = Const.SCREEN_SIZE * .06;
			
			_buttonsContainer = GameUtils.$.createContainer(Const.SCREEN_WIDTH, _buttonSize + 2 * padding, 0);
			
			var menuButton : ButtonSpecial = new ButtonSpecial(Main.$.assets.getTexture(Const.TEXTURE_HOME));
			_buttonsContainer.addChild(menuButton);
			menuButton.initTouchHandler(menuButtonHandler);
			menuButton.setSize(_buttonSize, _buttonSize * .8);
			LayoutUtils.$.top(menuButton, padding);
			LayoutUtils.$.left(menuButton, padding);
			
			_retryButton = new ButtonSpecial(Main.$.assets.getTexture(Const.TEXTURE_RETRY));
			_buttonsContainer.addChild(_retryButton);
			_retryButton.alignPivot();
			_retryButton.width = _retryButton.height= _buttonSize;
			_retryButton.initTouchHandler(retryButtonHandler);
			LayoutUtils.$.centralAlign(_retryButton,0);
			LayoutUtils.$.top(_retryButton, padding + _buttonSize * .5);
			_retryYCoord = _retryButton.y;
			
//			var soundButton : ButtonSpecial = new ButtonSpecial(Main.$.assets.getTexture(Const.TEXTURE_SOUND));
//			_buttonsContainer.addChild(soundButton);
//			soundButton.initTouchHandler(retryButtonHandler);
//			soundButton.setSize(_buttonSize, _buttonSize);
//			LayoutUtils.$.right(soundButton, padding);
//			LayoutUtils.$.top(soundButton, padding);
			
			addChild(_buttonsContainer);
			_buttonsContainer.y = -_buttonsContainer.height;
			
		}
		
		private function retryButtonHandler():void
		{
			_tween.tweenSize(_retryButton,.2, Const.TRANSITION_NAME, 0, _buttonSize, _buttonSize);
			_retryButton.alpha = 1;
			_currentLevel.resetLevel();
			ButtonSpecial.disable();
		}
		
		private function menuButtonHandler():void
		{
			Main.$.openMenu();
		}	
		
		private function startFirstLevel() : void
		{
			_currentLevel = LevelGenerator.$.currentLevel;
			_currentLevel.startLevel(_levelContainer);
		}
		
		
		
		private function levelCompletedHandler(e : Event):void
		{
			_currentLevel = LevelGenerator.$.nextLevel;
			_currentLevel.startLevel(_levelContainer);
			ButtonSpecial.disable();
			
		}
		
		private function gameOveredHandler(e : Event):void
		{
			var scaledSize : Number = _buttonSize * .6;
			_tween.tweenSizeInLoop(_retryButton, _buttonSize, scaledSize, _buttonSize, scaledSize, 1, 1, .6, .2);
		}
		
		private function resetLevelHandler(e : Event):void
		{
			_currentLevel.startLevel(_levelContainer);
		}
		
		override public function enterAnimation(dirtection:String):void
		{
			Main.mainContainer.addChild(this);
			super.enterAnimation(Const.DIRECTION_LEFT);
		}
		
		override public function exitAnimation(dirtection:String):void
		{
			super.exitAnimation(Const.DIRECTION_RIGHT);
			_tween.moveTo(_buttonsContainer, .3, -1, -_buttonsContainer.height, 0, Const.TRANSITION_NAME, 1);
		}
		
		override protected function exitAnimationComplete():void
		{
			_currentLevel.dispose();
		}
		
		override protected function enterAnimationComplete() : void
		{
			_tween.moveTo(_buttonsContainer, .3, -1, 0, 0, Const.TRANSITION_NAME, 1);
			startFirstLevel();
		}
	}
}