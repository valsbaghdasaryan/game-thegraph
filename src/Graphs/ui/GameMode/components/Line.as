package Graphs.ui.GameMode.components
{
	import Graphs.Main;
	import Graphs.ui.components.BaseComponent;
	
	import starling.animation.Transitions;
	import starling.display.Image;
	import starling.events.Event;
	
	public class Line extends BaseComponent
	{
		private var _dot1:Dot;
		private var _dot2:Dot;
		private var _lineHeight:Number;
		private var _lineWidth:Number;
		private var _isFull : Boolean;
		

		private var _angle:Number;
		private var _color:int;
		public function Line(dot1 : Dot, dot2 : Dot)
		{
			super();
			
			_dot1 = dot1;
			_dot2 = dot2;
			
			measure();
			_dot1.addLine(this);
			_dot2.addLine(this);
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event : Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			_lineHeight = dot1.size * .3;
			
			var lineImage : Image= new Image(Main.$.assets.getTexture(Const.TEXTURE_TUBE));
			addChild(lineImage);
			lineImage.width = _lineWidth;
			lineImage.height = _lineHeight;
			lineImage.pivotY = _lineHeight / 2 / lineImage.scaleY; // pivotY = _lineHeight /2  doesn't work 
			lineImage.color = _color;
			lineImage.rotation = _angle;
			
			this.x = dot1.xCoord;
			this.y = dot1.yCoord;
			
		}
		
		public function get dot1() : Dot
		{
			return _dot1;
		}
		
		private function measure() : void
		{
			var hasConnection : int = dot1.hasConnectionLine(dot2); 
			
			if(hasConnection)
			{
				_color = Const.COLOR_GREY_DARK;
				_isFull = true;
			}
			else
			{
				_color = Const.COLOR_GREY_LIGHT;
				_isFull = false;
			}
			
			var dify : Number = dot2.yCoord - dot1.yCoord;
			var difx : Number =  dot2.xCoord - dot1.xCoord;
			
			_angle = Math.atan2(dify,difx);
			_lineWidth = Math.sqrt(Math.pow(difx, 2) + Math.pow(dify, 2));
		}
		
		public function get isFull() : Boolean
		{
			return _isFull;
		}
		
		public function get dot2() : Dot
		{
			return _dot2;
		}
		
		override public function show(delay:Number):void{
			_tween.tween(this, .2, Transitions.EASE_IN_OUT, 1,1, delay,onCompleteHandler); 
			_isActive = true;
		}
		
		override public function hide():void
		{
			super.hide();
			_dot1.removeLineFromList(this);
			_dot2.removeLineFromList(this);
		}
		
		public function disposeLine() : void
		{
			this.removeChildren();
			_dot1 = null;
			_dot2 = null;
			this.parent.removeChild(this);
			super.dispose();
		}
		
		private function onCompleteHandler():void
		{
			dispatchEventWith(Const.READY);
		}
	}
}