package Graphs.ui.GameMode.components
{
	import flash.geom.Point;
	
	import Graphs.Main;
	import Graphs.ui.components.BaseComponent;
	import Graphs.utils.GameUtils;
	
	import starling.animation.Transitions;
	import starling.display.Image;
	import starling.events.Event;
	
	public class Dot extends BaseComponent
	{
		private static const DOTS_APPEAR_TIME : Number = .4;
		private static const DOT_COLOR_IDLE: Number = 0;
		private static const DOT_COLOR_ACTIVE: Number = 0;
		private static const DOT_COLOR_HINT: Number = 0x666666;
		
		private var _xCoord:Number;
		private var _yCoord:Number;
		private var _size:Number;
		private var _row : int;
		private var _column : int;
		private var _controller:DotController;
		private var _nearDots : Vector.<Dot>;
		private var _connectionLines : Vector.<Line>;
		private var _columnIndex:int;
		private var _isHintAnimationRunning:Boolean;
		private var _dotImage:Image;
		private var _outLinesNumber:int;
		private var _isOnTarget:Boolean;
		
		public function Dot( owner : DotController, xCoord : Number, yCoord : Number, size : Number, row : int, column : int, columnIndex : int, outLinesNumber : int )
		{
			super();

			_controller = owner;
			_xCoord = xCoord;
			_yCoord = yCoord;
			_size = size;
			_row = row;
			_column = column;
			_columnIndex = columnIndex;
			_outLinesNumber = outLinesNumber;
			
			_connectionLines = new Vector.<Line>;
			
			_controller.addEventListener(Const.WRONG_DOT, wrongDotPushedHandler);
			_controller.addEventListener(Const.DOT_PUSHED, anotherDotPushedHandler);
			_controller.addEventListener(Const.DOT_IS_FULL, dotIsFullHandler);
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		public function get columnIndex():int
		{
			return _columnIndex;
		}

		public function get yCoord():Number
		{
			return _yCoord;
		}

		public function get xCoord():Number
		{
			return _xCoord;
		}
		
		private function initialize():void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			_dotImage = new Image(Main.$.assets.getTexture(Const.TEXTURE_DOT));
			_dotImage.width = _dotImage.height = size;
			addChild(_dotImage);
			_dotImage.alignPivot();
			_dotImage.color = DOT_COLOR_IDLE;
			
			this.width = this.height = _size;
		}
		
		public function hasCollision(touchPoint : Point) : Boolean
		{
			var xProection : Number = touchPoint.x - this.x;
			var yProection : Number = touchPoint.y - this.y; 
			var distance : Number = Math.sqrt(Math.pow(xProection, 2) + Math.pow(yProection, 2));
			
			
			// size / 2 is radious 
			if(distance <= (_size / 2) * 1.2)
			{
				return true;
			}
			
			return false;
		}
		
		public function resetFromSwiping() : void
		{
			_isOnTarget = false;
		}
		
		private function anotherDotPushedHandler(e : Event):void
		{
			//  check if pushed dot isn't current dot then remove hint animation 
			if(this == _controller.pushedDot || !_isActive)
			{
				return;
			}
			if(hasConnectionLine(_controller.pushedDot))
			{
				hintAnimation();
			}
			else
			{
				deactiveAnimation();
				_isHintAnimationRunning = false
			}
		}
		
		private function wrongDotPushedHandler(e : Event):void
		{
			// TODO 
		}
		
		private function dotIsFullHandler(e : Event):void
		{
			var fullDot : Dot = e.data as Dot;
			if(fullDot != this && _nearDots)
			{
				for( var i : int = 0; i < _nearDots.length;i ++)
				{
					if(_nearDots[i] == fullDot)
					{
						_nearDots.splice(i, 1);
					}
				}
			}
		}
		
		public function isGettingFull() : Boolean
		{
			if(_outLinesNumber == 1 || _outLinesNumber - 1 > _connectionLines.length)
			{
				return false;
			}
			return  true;
		}
		
		public function getNearDotToConnect() : Dot
		{
			// if dot is getting full, than we must not connect with that dot 
			for each(var dot : Dot in _nearDots)
			{
				if(!hasConnectionLine(dot) && !dot.isGettingFull())
				{
					return dot;
				}
			}
			
			// if there isn't dot with no connection , than we should connect dot with 1 connection line
			for each(dot in _nearDots)
			{
				if(hasConnectionLine(dot) == 1 && !dot.isGettingFull())
				{
					return dot;
				}
			}
			
			return null;
		}
		
		public function push() : void
		{
			this.color = DOT_COLOR_ACTIVE;
			_controller.pushedDot = this;
			_tween.tweenInLoop(this, .8, 1, 1, 1.15, .4);
			_controller.dispatchEventWith(Const.DOT_PUSHED, false, this);
			trace("Dot : Push - pushing current dot [ " + _row  + " , " + _columnIndex + " ]");
			
			if(hasConnectionLine() == 0)
			{
				_controller.dispatchEventWith(Const.NO_CONNECTED_DOTS);
				deactiveAnimation();
			}
		}
		
		public function toSwipe() : void
		{
			_isOnTarget = true;
		}
		
		public function isTouchDisplay(image : Image) : Boolean 
		{
			if(image == _dotImage)
			{
				return true;
			}
			return false;
		}
			 
		public function unPushDot():void
		{
			if(!hasConnectionLine())
			{
				hide();
			}
			else
			{
				deactiveAnimation();
			}
		}
		
		public function hasConnectionLine(dot : Dot = null) : int
		{
			var count : int = 0;
			
			if(_connectionLines.length)
			{
				// if there isn't dot to check, and lines length is not 0, then it has connection
				if(dot == null)
				{
					return _connectionLines.length;
				}
				
				for each(var line : Line in _connectionLines)
				{
					if((line.dot1 == dot && line.dot2 == this) || (line.dot2 == dot && line.dot1 == this))
					{
						count ++;
					}
				}
			}
			
			return count;
		}
		
		public function removeConnectionLine(dot :Dot) : void
		{
			var wantedLine : Line; 
			// there can be more than one lines connected with current dots, we need the last one
			for each( var line : Line in _connectionLines)
			{
				if((line.dot1 == this && line.dot2 ==  dot) || (line.dot2 == this && line.dot1 == dot) )
				{
					wantedLine = line;
				}
			}
			
			wantedLine.hide();
		}
		
		public function removeLineFromList(deletingLine : Line) : void
		{
			for (var i : int ; i < _connectionLines.length; i ++)
			{
				if(_connectionLines[i] == deletingLine)
				{
					_connectionLines.splice(i, 1);
					return
				}
			}
		}
		
		private function hintAnimation() : void
		{
			this.color = DOT_COLOR_HINT;
			_isHintAnimationRunning = true;
		}
		
		private function deactiveAnimation() : void
		{
			this.color = DOT_COLOR_IDLE;
			_tween.tween(this, .2, Transitions.EASE_IN_OUT, 1, 1, 0.2);
		}
		
		public  function set color(value : uint) : void
		{
			_dotImage.color = value;
		}
		
		public function get color() : uint
		{
			return _dotImage.color;
		}
		
		private function hideDotRandomPlace() : void
		{
			var point : Point = globalToLocal(new Point(_xCoord, - _size));
			this.x = GameUtils.$.getRandomNumber(-Const.SCREEN_WIDTH, Const.SCREEN_WIDTH);
			this.y = point.y;
		}
		
		override public function show(delay : Number) : void
		{
			if(hasConnectionLine())
			{
				hideDotRandomPlace();
				_tween.moveTo(this, DOTS_APPEAR_TIME, _xCoord, _yCoord,delay, Const.TRANSITION_NAME, 1, function onComplete() : void
				{
					_controller.dispatchEventWith(Const.READY);
					_isActive = true;
				});
				_isActive = true;
			}
			else
			{
				_controller.dispatchEventWith(Const.READY);
			}
		}
		
		override public function hide():void
		{
			_tween.tween(this, .2, Transitions.EASE_IN_OUT, 0,1, 0.1);
			_isActive = false;
			this.removeEventListeners();
		}
		
		public function get row() : int
		{
			return _row;
		}
		
		public function get nearDots():Vector.<Dot>
		{
			return _nearDots;
		}
		
		public function get size() : int
		{
			return _size;
		}
		
		public function set nearDots(value:Vector.<Dot>):void
		{
			_nearDots = value;
		}
		
		public function disposeDot() : void
		{
			_nearDots = null;
			_connectionLines = null;
			_controller.removeEventListener(Const.WRONG_DOT, wrongDotPushedHandler);
			_controller.removeEventListener(Const.DOT_PUSHED, anotherDotPushedHandler);
			_controller.removeEventListener(Const.DOT_IS_FULL, dotIsFullHandler);
			this.removeEventListeners();
			_controller = null;
			this.removeChildren();
			this.parent.removeChild(this);
			super.dispose();
		}
		
		public function addLine(line : Line) : void
		{
			_connectionLines.push(line);
			if(_outLinesNumber > 1 && _connectionLines.length >= _outLinesNumber)
			{
				_controller.dispatchEventWith(Const.DOT_IS_FULL, false, this);
			}
		}
	}
}