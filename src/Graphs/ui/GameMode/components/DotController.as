package Graphs.ui.GameMode.components
{
	import flash.geom.Point;
	
	import Graphs.ui.GameMode.levels.LevelDataVO;
	import Graphs.ui.components.TweenSpecial;
	import Graphs.utils.GameUtils;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class DotController extends EventDispatcher
	{
		protected var _pushedDot : Dot;
		protected var _dots:Vector.<Dot>;
		protected var _readyDots:int;
		protected var _lines:Vector.<Line>;
		protected var _linesCount:int;
		protected var _linesReady:int;
		protected var _tween : TweenSpecial;
		protected var _lineContainer:Sprite;
		protected var _dotsContainer:Sprite;
		
		private var _connectionSequence:Vector.<Dot>;
		private var _swipingDot:Dot;
		
		public function DotController(linesCount : int)
		{
			_linesCount = linesCount;
			_tween = new TweenSpecial;
			super();
		}

		public function get pushedDot() : Dot
		{
			return _pushedDot;
		}
		
		public function set pushedDot(value : Dot) : void
		{
			_pushedDot = value;
		}
		
		protected function initDots(levelData : LevelDataVO) : void
		{
			var horizontalStep : Number;
			var verticalStep : Number = Const.GAME_HEIGHT / (levelData.dotsData.length + 1);
			var size : Number = levelData.dotSize;
			var xCoord : Number;
			var yCoord : Number;
			var colu8mnIndex : int;
			
			_dots = new Vector.<Dot>;
			
			addEventListener(Const.READY, dotReadyHandler);
			
			for(var i : int = 0; i < levelData.dotsData.length; i ++)
			{
				colu8mnIndex = 0;
				horizontalStep = Const.SCREEN_WIDTH / (levelData.dotsData[i].length + 1);
				yCoord = verticalStep + verticalStep * i;
				
				for(var j : int = 0; j < levelData.dotsData[i].length; j ++)
				{
					if(levelData.dotsData[i][j])
					{
						xCoord = horizontalStep + horizontalStep * j;
						colu8mnIndex ++;
						
						var dot : Dot = new Dot(this , xCoord, yCoord, size, i + 1, j + 1, colu8mnIndex, levelData.dotsData[i][j]);
						_dots.push(dot);	
					}
				}
			}
			
			for each(dot in _dots)
			{
				dot.nearDots = getNearDots(dot);
			}
		}
		
		protected function lineReady(e : Event):void
		{
			_linesReady ++;
			if(_linesReady == _lines.length)
			{
				dispatchEventWith(Const.ALL_LINES_READY);
			}
		}
		
		protected function dotReadyHandler( e : Event):void
		{
			_readyDots ++;
			if(_readyDots == _dots.length)
			{
				dispatchEventWith(Const.ALL_DOTS_READY);
			}
		}
		
		protected function touchHandler(e : TouchEvent) : void
		{
			var touch : Touch = e.getTouch(e.currentTarget as DisplayObject);
			var dot : Dot = getDot(e.target as DisplayObject);
			
			if(touch && dot)
			{
				if(touch.phase == TouchPhase.BEGAN)
				{
					pushTheDot(dot);
				}
				
				if(touch.phase == TouchPhase.MOVED && _swipingDot != null)
				{
					var swipedDot : Dot = checkForSwipe(touch);
					if(swipedDot)
					{
						pushTheDot(swipedDot);
					}
				}
				
				if(touch.phase == TouchPhase.ENDED)
				{
					clearSwipingDot();
				}
			}
		}
		
		private function getDot(target : DisplayObject) : Dot
		{
			var dotImage : Image = target as Image;
			if(dotImage == null)
			{
				return null;
			}
			
			for each( var dot : Dot in _dots)
			{
				if(dot.isTouchDisplay(dotImage))
				{
					return dot;
				}
			}
			
			return null;
		}
		
		private function dotToSwipe(dot : Dot) : void
		{
			clearSwipingDot();
			
			_swipingDot = dot;
			dot.toSwipe();
		}
		
		private function clearSwipingDot() : void
		{
			if(_swipingDot)
			{
				_swipingDot.resetFromSwiping();
				_swipingDot = null;
			}
		}
		
		private function singleDotsConnection() : void
		{
			var changing : int = 1;
			for(var i : int = 0; i < changing; i ++)
			{
				for each(var dot : Dot in _dots)
				{
					if(dot.hasConnectionLine() == 1 && dot.getNearDotToConnect() != null)
					{
						connectTwoDots(dot, dot.getNearDotToConnect());
						changing ++;
					}
				}
			}
		}
			
		private function sortLines() : void
		{
			var line : Line;
			var i : int = 0;
			var count : int = 0;
			
			while(count < _lines.length)
			{
				count ++;
				
				if(_lines[i].isFull)
				{
					line = _lines.splice(i, 1)[0];
					_lines.push(line);
				}
				else
				{
					i++;
				}
			}
		}
		
		protected function connectTheDots() : void
		{
			var currentDot : Dot = getRandomDotFrom(_dots);
			var firstConnectedDot : Dot = currentDot;
			var count : int = 0;
			var secondDot : Dot;
			
			_lines = new Vector.<Line>;
			
			while (count < _linesCount)
			{
				secondDot = currentDot.getNearDotToConnect();
				if(secondDot == null)
				{
					//if there is no free dot to connect, then continue connection from the first connected dot
					if(currentDot != firstConnectedDot && firstConnectedDot.getNearDotToConnect()) 
					{
						currentDot = firstConnectedDot;
						secondDot = firstConnectedDot.getNearDotToConnect();
					}
					else
					{
						break;
					}
				}
				connectTwoDots(currentDot, secondDot);
				
				currentDot = secondDot;
				secondDot = null
				count ++;
			}
			currentDot = null;
			
			singleDotsConnection();
			sortLines();
			trace("line expected: " + _linesCount + "\nline result: " + _lines.length);
		}
		
		private function pushTheDot(dot : Dot) : void
		{
			if(_pushedDot)
			{
				if(dot.hasConnectionLine(_pushedDot))
				{
					dot.removeConnectionLine(_pushedDot);
					_pushedDot.unPushDot();
					dot.push();
					dotToSwipe(dot);
				}
				else if(dot == _pushedDot)
				{ 
					dotToSwipe(dot);
				}
				else
				{
					dispatchEventWith(Const.WRONG_DOT);
				}
			}
			else
			{
				dot.push();
				dotToSwipe(dot);
			}
		}
		
		private function checkForSwipe(touch : Touch): Dot
		{
			var touchPoint : Point = _dotsContainer.globalToLocal(new Point(touch.globalX, touch.globalY));
			
			for each(var dot : Dot in _dots)
			{
				if(dot != _pushedDot && dot.hasCollision(touchPoint) && dot.hasConnectionLine(_swipingDot))
				{
					return dot;
				}
			}
		
			return null;
		}
		
		protected function disposeDots() : void
		{
			for each ( var dot : Dot in _dots)
			{
				dot.disposeDot();
			}
			_dots = null;
			_pushedDot = null;
			_swipingDot = null;
		}
		
		protected function disposeLines() : void
		{
			for each ( var line : Line in _lines)
			{
				line.disposeLine();
			}
			
			_lines = null;
		}
		
		private function getNearDots(currentDot : Dot) : Vector.<Dot>
		{
			var row : int = currentDot.row;
			var columnIndex : int = currentDot.columnIndex;
			var nearDots : Vector.<Dot> = new Vector.<Dot>;
			
			for each(var dot : Dot  in _dots)
			{
				if(dot == currentDot)
				{
					continue;
				}
				if(dot.row == row && (dot.columnIndex == columnIndex - 1 || dot.columnIndex == columnIndex + 1))
				{
					nearDots.push(dot);
				}
				else if(dot.row == row - 1 || dot.row == row + 1)
				{
					nearDots.push(dot);
				}
			}
			
			return GameUtils.$.shuffle(nearDots);
		}
		
		public function getRandomDotFrom(dots : Vector.<Dot>) : Dot
		{
			var index : int = GameUtils.$.getRandomInteger(0, dots.length);
			return dots[index];
		}
		
		private function connectTwoDots(dot1 : Dot, dot2 : Dot) : void
		{
			var line : Line = new Line(dot1, dot2);
			line.addEventListener(Const.READY, lineReady);
			_lines.push(line);
		}
	}
}