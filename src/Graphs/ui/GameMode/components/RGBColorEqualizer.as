package Graphs.ui.GameMode.components
{
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;

	public class RGBColorEqualizer
	{
		private var _target:Image;
		private var _time:Number;
		private var _coeficent:int = 0;
		
		
		public function RGBColorEqualizer(target : Image, time : Number = .9 )
		{
			_target = target;
			_time = time;
		}
		
		public function blinkColor() : void
		{
			Starling.juggler.removeTweens(this);
			
			
			var tween : Tween = new Tween(this, _time, Transitions.EASE_IN_OUT);
			tween.animate("rgbColor",100);
			tween.onComplete = function () : void
			{
				blinkColor();
//				Starling.juggler.removeTweens(this);
//				
//				var tweenLoop : Tween = new Tween(this, _time, Transitions.EASE_IN_OUT);
//				tweenLoop.animate("rgbColor",0);
//				tweenLoop.onComplete = blinkColor;
//				Starling.juggler.add(tweenLoop);
			};
			
			Starling.juggler.add(tween);
			
		}
		
		
		public function stopBlink(color : Number = 0) : void
		{
			this.color = color;
			Starling.juggler.removeTweens(this);
		}
		
		public function set color(value : Number) : void
		{
			_target.color = value;
		}
		
		public function set rgbColor(value : Number) : void
		{
			_coeficent = value / 100 * 55;
			var currentNumber : Number = 0;
			var hex : String;
			if(_coeficent < 10)
			{
				hex = "0" + _coeficent.toString();
			}
			else
			{
				hex = _coeficent.toString();
			}
			currentNumber = red(currentNumber, hex);
			currentNumber = green(currentNumber, hex);
			currentNumber = blue(currentNumber, hex);
			
			this.color = currentNumber;
		}
		
		public function get rgbColor() : Number
		{
			return _coeficent;
		}
		
		private function red(number : Number, to : String) : Number
		{
			var digits : Array = to.split("");
			number += Math.pow(16, 5) * int(digits[0]) + Math.pow(16, 4) * (digits[1]);
			return number;
		}
		
		private function green(number : Number, to : String) : Number
		{
			var digits : Array = to.split("");
			number += Math.pow(16, 3) * (digits[0]) + Math.pow(16, 2) * (digits[1]);
			return number;
		}
		
		private function blue(number : Number, to : String) : Number
		{
			var digits : Array = to.split("");
			number += 16 * int(digits[0]) + int(digits[1]);
			return number;
		}
	}
}