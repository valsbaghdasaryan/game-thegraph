package Graphs.ui.GameMode.levels
{
	import Graphs.ui.GameMode.components.Dot;
	import Graphs.ui.GameMode.components.DotController;
	import Graphs.ui.GameMode.components.Line;
	import Graphs.ui.components.ButtonSpecial;
	import Graphs.utils.GameUtils;
	
	import starling.animation.Transitions;
	import starling.display.DisplayObjectContainer;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;

	public class Level extends DotController 
	{
		private var _levelDataVO:LevelDataVO;
		private var _levelIndex : int;

		private var _gameContainer:Sprite;
		private var _level:TextField;
		
		public function Level(levelDataVO : LevelDataVO, levelIndex : int)
		{
			super(levelDataVO.linesCount);
			
			_levelDataVO = levelDataVO;
			_levelIndex = levelIndex;
			
			
			
			super.addEventListener(Const.ALL_DOTS_READY, allDotsAreReady);
			super.addEventListener(Const.ALL_LINES_READY, addTouchListener);
			super.addEventListener(Const.NO_CONNECTED_DOTS, noConnectionDotHandler);
			
			_gameContainer = GameUtils.$.createContainer(Const.SCREEN_WIDTH, Const.GAME_HEIGHT, 0, Const.COLOR_GREY_LIGHT);
			_lineContainer = GameUtils.$.createContainer(Const.SCREEN_WIDTH, Const.GAME_HEIGHT, 0);
			_dotsContainer = GameUtils.$.createContainer(Const.SCREEN_WIDTH, Const.GAME_HEIGHT, 0);
			
			_gameContainer.addChild(_lineContainer);
			_gameContainer.addChild(_dotsContainer);
			
			initLevelText();
			updateDotsSize();
		}
		
		private function updateDotsSize() : void
		{
			var size : Number = Const.GAME_HEIGHT * 0.11;
			var max : int =  _levelDataVO.dotsData.length;			
			for(var i : int = 0; i < _levelDataVO.dotsData.length; i++)
			{
				if(_levelDataVO.dotsData[i].length > max)
				{
					max = _levelDataVO.dotsData[i].length;
				}
			}
			
			if(max > 3)
			{
				size -= (max - 3) * size*.1;
			}
			
			_levelDataVO.dotSize = size;
		}
		
		private function initLevelText() : void
		{
			var fontSize : int = Const.SCREEN_WIDTH / 2;
			_level = new TextField(Const.SCREEN_WIDTH, Const.SCREEN_HEIGHT, (_levelIndex + 1).toString(), "Verdana", fontSize, Const.COLOR_GREY_DARK);
			_gameContainer.addChild(_level);
			_level.visible = false;
			_level.pivotX = _level.width / 2;
			_level.pivotY = _level.height/ 2;
		}
		
		private function addTouchListener(e : Event):void
		{
			ButtonSpecial.enable();
			_dotsContainer.addEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		private function allDotsAreReady():void
		{
			showLines(_lineContainer);
		}
		
		public function startLevel(gameView : DisplayObjectContainer) : void
		{
			trace("Level : startLevel " + _levelIndex );
			
			initDots(_levelDataVO);
			connectTheDots();
			
			_readyDots = 0;
			_linesReady = 0;
			
			gameView.addChild(_gameContainer);
			_gameContainer.y = Const.SCREEN_HEIGHT - Const.GAME_HEIGHT;
			
			startLevelAnimation();
		}
		
		private function startLevelAnimation():void
		{
			_level.visible = true;
			
			_level.x = Const.SCREEN_WIDTH *.5;
			_level.y = Const.SCREEN_HEIGHT * .35;
			_level.alpha = 1;
			_level.scaleX = _level.scaleY = 0;
			_tween.tween(_level, .7, Transitions.EASE_IN, 0,1.5, 0, levelAnimationCompleted);
		}
		
		private function levelAnimationCompleted():void
		{
			_level.visible = false;
			showDots(_dotsContainer);			
		}
		
		private function noConnectionDotHandler(e : Event):void
		{
			var activeDotsCount : int = 0;
			for each ( var dot : Dot in _dots)
			{
				if(dot.isActive)
				{
					activeDotsCount ++
				}
				
				if(activeDotsCount >= 2)
				{
					// if there is more than one active dots, then game over
					gameOver()
					return;
				}
			}
			
			//if the isnt active dots more than 1, then level completed
			levelCompleted();
		}
		
		private function showDots(container : Sprite):void
		{
			var delay : Number = 0;
			
			var deltaDelay : Number = Const.TRANSITION_TIME / _dots.length;
			
			for each ( var dot : Dot in _dots)
			{
				container.addChild(dot);
				
				dot.show(delay);
				delay += deltaDelay;
			}
			
		}
		
		private function showLines(container : Sprite):void
		{
			var delay : Number = .1;
			var deltaDelay : Number = Const.TRANSITION_TIME / (_lines.length); 
			
			for each ( var line : Line in _lines)
			{
				container.addChild(line);
				
				line.show(delay);
				delay += deltaDelay
			}
		}
		
		public function dispose() : void
		{
			disposeLines();
			disposeDots();
			if(_gameContainer.parent)
			{
				_gameContainer.parent.removeChild(_gameContainer);
			}
		}
		
		private function levelCompleted() : void
		{
			dispose();
			LevelGenerator.$.dispatchLevelCompleted()
			trace("levelCompleted");
		}
		
		private function gameOver() : void
		{
			LevelGenerator.$.dispatchGameOvered();
		}
		
		public function resetLevel() : void
		{
			dispose();
			
			LevelGenerator.$.dispatchLevelReset();
		}
			
	}
}