package Graphs.ui.GameMode.levels
{
	public class LevelParser
	{
		public function LevelParser()
		{
			
		}
		
		public function parse(object : Object) : Vector.<LevelDataVO>
		{
			var parsedData : Object = object;
			var levels : Vector.<LevelDataVO> = new Vector.<LevelDataVO>;
			var level : LevelDataVO;
			
			for(var i : int = 0; i < parsedData.levels.length; i ++)
			{
				
				level = new LevelDataVO;
				level.dotsData = parsedData.levels[i].boards[0];
				level.linesCount = parsedData.levels[i].steps;
				level.levelIndex = parsedData.levels[i].levelIndex;
				levels.push(level);
			}
			
			return levels;
		}
	}
}