package Graphs.ui.GameMode.levels
{
	import flash.net.SharedObject;
	
	import Graphs.Main;
	
	import starling.display.Sprite;
	import starling.events.EventDispatcher;

	public class LevelGenerator extends EventDispatcher
	{
		private static const LEVEL_SHARED_OBJECT_NAME : String = "LevelSharedObject";
		
		private static var _instance:LevelGenerator;

		private var _levels:Vector.<Level>;
		private var _currentLevelIndex:int;
		private var _parent : Sprite;
		
		private var _levelUnlocked : int = 1;
		private var _unlockedLevels:int;
		
		private var _sharedObject : SharedObject;
		
		public static function get $():LevelGenerator
		{
			if(!_instance)
			{
				_instance = new LevelGenerator();
			}
			return _instance;
		}
		
		public function LevelGenerator()
		{
			if(_instance) {
				throw new Error("another LevelGenerator exists");
			}
			
			_sharedObject = SharedObject.getLocal(LEVEL_SHARED_OBJECT_NAME);
			
			if(_sharedObject.data.unlockedLevels >= 1)
			{
				_unlockedLevels = _sharedObject.data.unlockedLevels; 
			}
			else
			{
				_unlockedLevels = 1;
				saveUnlockLevels();
			}
				
		}
		
		private function saveUnlockLevels() : void
		{
			_sharedObject.data.unlockedLevels = _unlockedLevels;
			_sharedObject.flush();
		}
			
		
		public function selectLevel(level : int) : void
		{
			if(level >= 0)
			{
				_currentLevelIndex = level;
			}
			else
			{
				throw new Error("level index must be positive");
			}
		}
			
		
		public function parseLevels():void
		{
			var levelParser : LevelParser = new LevelParser;
			var levelsDataVO : Vector.<LevelDataVO> = levelParser.parse(Main.$.assets.getObject("levels"));
			
			_levels = new Vector.<Level>;
			for(var i : int = 0; i < levelsDataVO.length; i ++)
			{
				var level : Level = new Level(levelsDataVO[i], i);
				_levels.push(level);
			}
			
			
			// set level index to 0 as first level
			_currentLevelIndex = 0;
			
			dispatchEventWith(Const.LEVELS_LOADED);
		}
		
		public function get unlockedLevels() : int 
		{
			return _unlockedLevels;
		}
		public function set unlockedLevels(value : int) : void
		{
			_unlockedLevels = value;
			saveUnlockLevels();
		}
		
		public function get levelsLength() : uint
		{
			return _levels.length;
		}
		
		public function dispatchLevelReset() : void
		{
			dispatchEventWith(Const.RESET_LEVEL);
		}
		
		public function dispatchLevelCompleted() : void
		{
			if(_unlockedLevels <= _currentLevelIndex + 1)
			{
				unlockedLevels = _currentLevelIndex + 2;
				
				dispatchEventWith(Const.UNLOCKED_NUMBER_CHANGED);
			}
			dispatchEventWith(Const.LEVEL_COMPLETED);
		}
		
		public function dispatchGameOvered() : void
		{
			dispatchEventWith(Const.GAME_OVERED);
		}
		
		public function get nextLevel() : Level
		{
			_currentLevelIndex ++;
			return _levels[_currentLevelIndex];
		}
		
		
		
		public function get currentLevel() : Level
		{
			return _levels[_currentLevelIndex];
		}
		
	}
}