package Graphs.ui.components
{
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;

	public class BaseMode extends Sprite
	{
		protected var _transitionName : String = Transitions.EASE_IN;
		protected var _tween:TweenSpecial;
		
		public function BaseMode()
		{
			super();
			_tween = new TweenSpecial;
		}
		
		protected function initBg():void
		{
			var quad : Quad = new Quad(Const.SCREEN_WIDTH, Const.SCREEN_HEIGHT, Const.COLOR_BG); 
			addChild(quad);
		}
		
		public function enterAnimation(dirtection : String) : void
		{
			ButtonSpecial.disable();
			switch(dirtection)
			{		
				case Const.DIRECTION_LEFT:
				{
					this.x = Const.SCREEN_WIDTH + this.width;
					this.y = 0;
					break;
				}
				case Const.DIRECTION_RIGHT:
				{
					this.x = - this.width;
					this.y = 0;
					break;
				}
				case Const.DIRECTION_UP:
				{
					this.x = 0;
					this.y = Const.SCREEN_HEIGHT + this.height;
					break;
				}
				case Const.DIRECTION_DOWN:
				{
					this.x = 0;
					this.y = - this.height;
					break;
				}
			}
			
			var transitionTween:Tween = new Tween(this, Const.TRANSITION_TIME, _transitionName);
			transitionTween.animate("x", 0);
			transitionTween.animate("y", 0);
			transitionTween.onComplete = enterAnimationComplete;
			Starling.juggler.add(transitionTween);
		}
		
		protected function enterAnimationComplete() : void
		{
			ButtonSpecial.enable();
		}
		
		public function exitAnimation(dirtection : String ) : void
		{
			ButtonSpecial.disable();
			this.x = 0;
			this.y = 0;
			
			var xCoord : int;
			var yCoord : int;
			
			switch(dirtection)
			{		
				case Const.DIRECTION_LEFT:
				{
					xCoord = - this.width;
					yCoord = 0;					
					break;
				}
				case Const.DIRECTION_RIGHT:
				{
					xCoord = Const.SCREEN_WIDTH + this.width;
					yCoord = 0;
					break;
				}
				case Const.DIRECTION_UP:
				{
					xCoord = 0;
					yCoord = - this.height;
					break;
				}
				case Const.DIRECTION_DOWN:
				{
					xCoord = 0;
					yCoord = Const.SCREEN_HEIGHT + this.height;
					break;
				}
			}
			
			var transitionTween : Tween = new Tween(this, Const.TRANSITION_TIME, _transitionName);
			transitionTween.animate("x", xCoord);
			transitionTween.animate("y", yCoord);
			transitionTween.onComplete = exitAnimationComplete; 
			
			Starling.juggler.removeTweens(this);
			Starling.juggler.add(transitionTween);
		}
		
		protected function exitAnimationComplete() : void
		{
			ButtonSpecial.disable();
			this.parent.removeChild(this);
		}
	}
}