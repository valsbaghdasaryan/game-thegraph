package Graphs.ui.components
{
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObject;

	public class TweenSpecial
	{
		public function TweenSpecial()
		{
		}
		
		public function tweenInLoop(target : DisplayObject, 
									scaleFrom : Number = 1,
									scaleTo: Number = 1,
									fadeFrom : Number = 0,
									fadeTo: Number = 1,
									time : Number = .5,
									delay : Number = 0,
									transition : String = Transitions.EASE_IN_OUT) : void
		{
			Starling.juggler.removeTweens(target);
			
			var tween2 : Tween = new Tween(target, time,transition);
			tween2.fadeTo(fadeFrom);
			
			var tween1 : Tween = new Tween(target, time,transition);
			tween1.fadeTo(fadeTo);
			
			if(!(scaleFrom == scaleTo == target.scaleX == target.scaleY))
			{
				tween2.scaleTo(scaleFrom);
				tween1.scaleTo(scaleTo);
			}
			
			tween2.nextTween = tween1;
			
			tween1.onComplete = function onComplete(): void
			{
				tweenInLoop(target,scaleFrom, scaleTo, fadeFrom, fadeTo, time, delay, transition);
			}
			
			Starling.juggler.add(tween2);
		}
		
		public function tweenSizeInLoop(target : DisplayObject, 
									widthFrom : Number,
									widthTo: Number,
									heightFrom : Number,
									heightTo : Number,
									fadeFrom : Number = 0,
									fadeTo: Number = 1,
									time : Number = .5,
									delay : Number = 0,
									transition : String = Transitions.EASE_IN_OUT) : void
		{
			Starling.juggler.removeTweens(target);
			
			var tween2 : Tween = new Tween(target, time,transition);
			tween2.fadeTo(fadeFrom);
			tween2.animate("width", widthFrom);
			tween2.animate("height", heightFrom);
			
			var tween1 : Tween = new Tween(target, time,transition);
			tween1.fadeTo(fadeTo);
			tween1.animate("width", widthTo);
			tween1.animate("height", heightTo);
			
			
			tween2.nextTween = tween1;
			
			tween1.onComplete = function onComplete(): void
			{
				tweenSizeInLoop(target,widthFrom, widthTo, heightFrom, heightTo, fadeFrom, fadeTo, time, delay, transition);
			}
			
			Starling.juggler.add(tween2);
		}
		
		public function moveTo(target : DisplayObject,
							   animationTime : Number,
							   xCoord : Number = -1,
							   yCoord : Number = -1,
							   delay : Number = 0,
							   transition : String = "linear",
							   fadeIn : Number = 1,
							   tweenComplete : Function = null) : void
		{
			var tween : Tween = new Tween(target, animationTime, transition);
			tween.delay = delay;
			
			if(tweenComplete)
			{
				tween.onComplete = tweenComplete;             
			}
			
			if(fadeIn != -1)
			{
				tween.fadeTo(fadeIn);
			}
			
			if(xCoord == -1)
			{
				xCoord = target.x;
			}
			
			if(yCoord == -1)
			{
				yCoord = target.y;
			}
			
			tween.animate("x", xCoord);
			tween.animate("y", yCoord);
			
			Starling.juggler.removeTweens(target);
			Starling.juggler.add(tween);
		}
		
		public function tweenSize(target : DisplayObject, time : Number, transition : String, delay : Number, width : Number, height : Number, onComplete : Function = null): void
		{
			Starling.juggler.removeTweens(target);
			var tween2 : Tween = new Tween(target, time,transition);
			tween2.delay = delay;
			tween2.animate("width", width);
			tween2.animate("height", height);
			if(onComplete)
			{
				tween2.onComplete = onComplete;
			}
			
			Starling.juggler.add(tween2);
		}
		
		
		public function fadeInLoop(target : DisplayObject, fadeFrom : Number = .5, fadeTo : Number = 1, time : Number = .2):void
		{
			Starling.juggler.removeTweens(target);
			
			var tween2 : Tween = new Tween(target, time,Const.TRANSITION_NAME);
			tween2.fadeTo(fadeFrom);
			
			var tween1 : Tween = new Tween(target, time,Const.TRANSITION_NAME);
			tween1.fadeTo(fadeTo);
			
			tween2.nextTween = tween1;
			
			tween1.onComplete = function onComplete(): void
			{
				fadeInLoop(target,fadeFrom, fadeTo, time);
			}
			
			Starling.juggler.add(tween2);
		}
		
		public function tween(target, time, transition = Transitions.EASE_IN_OUT, fadeTo = 1, scaleTo = 1, delay : Number = 0, onComplete : Function = null) : void
		{
			Starling.juggler.removeTweens(target);
			
			var tween2 : Tween = new Tween(target, time,transition);
			tween2.fadeTo(fadeTo);
			tween2.scaleTo(scaleTo);
			tween2.delay = delay;
			if(onComplete)
			{
				tween2.onComplete = onComplete;
			}
			
			 Starling.juggler.add(tween2);
		}
		
		public function removeTweens(target : DisplayObject) : void
		{
			Starling.juggler.removeTweens(target);
		}
		
	}
}