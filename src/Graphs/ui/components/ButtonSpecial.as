package Graphs.ui.components
{
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	
	public class ButtonSpecial extends Button
	{
		private static var isEnabled : Boolean = true;
		
		private var _touchListener : Function = null;
		private var _touchPhase : String = null;
		
		public function ButtonSpecial(upState:Texture, text:String="", downState:Texture=null, overState:Texture=null, disabledState:Texture=null)
		{
			super(upState, text, downState, overState, disabledState);
		}
		
		public function initTouchHandler(touchListener : Function, phase : String = TouchPhase.ENDED) : void
		{
			_touchListener = touchListener; 
			_touchPhase = phase;
			addEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		private function touchHandler(e : TouchEvent):void
		{
			var touch : Touch = e.getTouch(e.currentTarget as DisplayObject);
			if(touch && touch.phase == _touchPhase && isEnabled)
			{
				_touchListener.call();
			}
		}
		
		public function setSize(width : int, height : int) : void
		{
			this.width = width;
			this.height = height;
		}
		
		public static function enable() : void
		{
			isEnabled = true;
		}
		
		public static function disable() : void
		{
			isEnabled = false;
		}
		
	}
}