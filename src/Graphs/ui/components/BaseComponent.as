package Graphs.ui.components
{
	
	import starling.animation.Transitions;
	import starling.display.Sprite;
	
	public class BaseComponent extends Sprite
	{
		protected var _tween:TweenSpecial;
		protected var _isActive:Boolean;
		
		
		public function BaseComponent()
		{
			super();
			
			this.alpha = 0;
			_tween = new TweenSpecial;
			_isActive = false;
		}
		
		public function show(delay : Number) : void
		{
			_tween.tween(this, .2, Transitions.EASE_IN_OUT, 1,1, delay); 
			_isActive = true;
		}
		
		public function hide() : void
		{
			_tween.tween(this, .2, Transitions.EASE_IN_OUT, 0,1);
			_isActive = false;
		}
		
		public function get isActive() : Boolean
		{
			return _isActive;
		}
		
		
		public function hideImmediately() : void
		{
			this.alpha = 0;
			_isActive = false;
		}
	}
}