package Graphs.ui.SelectLevelMode
{
	import Graphs.Main;
	import Graphs.ui.GameMode.levels.LevelGenerator;
	import Graphs.ui.SelectLevelMode.component.LevelPageViewController;
	import Graphs.ui.components.BaseMode;
	import Graphs.ui.components.IMode;
	
	import starling.events.Event;
	
	public class SelectLevelMode extends BaseMode implements IMode
	{

		private var _pageController:LevelPageViewController;
		public function SelectLevelMode()
		{
			super();

			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		protected function initialize(e : Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			initBg();
			initLevels();
		}
		
		private function initLevels():void
		{
			_pageController = new LevelPageViewController(Const.SCREEN_HEIGHT, LevelGenerator.$.levelsLength,5,4);
			_pageController.addEventListener(Const.LEVEL_SELECTED, onLevelSelectedHandler);
			addChild(_pageController);
		}
		
		private function onLevelSelectedHandler(e : Event):void
		{
			LevelGenerator.$.selectLevel(e.data as int);
			Main.$.startGame();
		}
		
		override public function enterAnimation(dirtection:String):void
		{
			Main.mainContainer.addChild(this);
			super.enterAnimation(dirtection);
		}
		
		override protected function enterAnimationComplete():void
		{
			super.enterAnimationComplete();
			
			_pageController.enterAnimation();
			_pageController.dispatchEventWith(Const.RESET);
		}
	}
}