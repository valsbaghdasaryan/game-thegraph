package Graphs.ui.SelectLevelMode.component
{
	public interface IPageController
	{
		function changePage(pageNumber : int) : void
	}
}