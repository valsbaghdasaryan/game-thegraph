package Graphs.ui.SelectLevelMode.component
{
	import Graphs.ui.GameMode.levels.LevelGenerator;
	import Graphs.ui.components.TweenSpecial;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class LevelPageViewController extends Sprite implements IPageController
	{
		private var _hegith:Number;
		private var _length:uint;
		private var _column:int;
		private var _row:int;

		private var _pages:Vector.<LevelPage>;
		private var _tween:TweenSpecial;

		private var _indicators:Vector.<PageIndicator>;

		private var _pageContainer:Sprite;
		private var _isSwiping:Boolean;
		private var _xTouch:Number;
		private var _touchBegan:Number;
		
		
		public function LevelPageViewController(height : Number, levelsLength : uint, row : int, column : int)
		{
			super();
			
			_hegith = height;
			_row = row;
			_column = column;
			_length = levelsLength;
				
			_tween = new TweenSpecial;
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		public function get levelLength() : uint
		{
			return _length
		}
		
		public function get unlockedLevel() : int
		{
			return LevelGenerator.$.unlockedLevels;
		}
		
		private function initialize(e : Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			initPages();
			initPageIndicator();
			
			if(_pages.length > 1)
			{
				_pageContainer.addEventListener(TouchEvent.TOUCH, onPageTouched);
			}
		}		
		
		private function initPages():void
		{
			_pages = new Vector.<LevelPage>;
			_pageContainer = new Sprite;
			addChild(_pageContainer);
			
			var pageCapacity : int = _row * _column;
			var pageHeight : Number = _hegith * .9;
			var pageCount : int = (_length - 1) / pageCapacity + 1;
			var page : LevelPage;
			
			for(var i :int = 0; i < pageCount; i ++)
			{
				page = new LevelPage(this, i + 1, _row, _column, pageHeight);
				_pageContainer.addChild(page);
				page.x = Const.SCREEN_WIDTH * i;
				_pages.push(page);
				
			}
		}
		
		private function onPageTouched(e : TouchEvent):void
		{
			var touch : Touch = e.getTouch(e.currentTarget as DisplayObject);
			
			if(touch)
			{
				if(touch.phase == TouchPhase.BEGAN)
				{
					_isSwiping = true;
					_touchBegan = _pageContainer.x;
					_xTouch = touch.globalX;
					
				}
				if(touch.phase == TouchPhase.ENDED)
				{
					if(_isSwiping)
					{
						var difference : Number = _touchBegan - _pageContainer.x;
						swipePage(difference);	
						_isSwiping = false;
					}
				}
				
				if(touch.phase == TouchPhase.MOVED && _isSwiping)
				{
					var proection : Number = touch.globalX - _xTouch;
					
					if(_pageContainer.x + proection <= 0 && _pageContainer.x + proection >= -Const.SCREEN_WIDTH * (_pages.length - 1))
					{
						_pageContainer.x += proection;
					}
					_xTouch = touch.globalX;
				}
			}
		}
		
		private function swipePage(difference : Number) : void
		{
			var param : int = Math.abs(_pageContainer.x) / Const.SCREEN_WIDTH;
			var minDifference : Number = Const.SCREEN_WIDTH * .3;
			
			if( (difference > 0 &&  Math.abs(difference) > minDifference) || (difference < 0 && Math.abs(difference) < minDifference))
			{
				changePage(param + 2)
			}
			else
			{
				changePage(param + 1);
			}
		}
		
		private function initPageIndicator():void
		{
			var indicatorContainer : Sprite = new Sprite;
			var padding : Number = Const.SCREEN_SIZE * .02;
			var size : Number = Const.SCREEN_SIZE * .03;
			addChild(indicatorContainer);
			
			var indicator : PageIndicator;
			
			_indicators = new Vector.<PageIndicator>;
			
			for(var i : int = 0; i < _pages.length; i++)
			{
				indicator = new PageIndicator(this, i + 1, size);
				indicatorContainer.addChild(indicator);
				indicator.x = i * (size + padding);
				
				_indicators.push(indicator);
			}
			_indicators[0].isSelected = true;
			
			indicatorContainer.x = (Const.SCREEN_WIDTH - indicatorContainer.width) /2;
			indicatorContainer.y = Const.SCREEN_HEIGHT * .93;
			if(_pages.length == 1)
			{
				indicatorContainer.visible = false;
			}
		}
		
		public function changePage(pageNumber : int) : void
		{
			var position : Number = (pageNumber - 1) * Const.SCREEN_WIDTH;
			var transitionTime : Number = Math.abs(position + _pageContainer.x) * .5 / Const.SCREEN_WIDTH ;
			_tween.moveTo(_pageContainer, transitionTime, -position, -1, 0, Const.TRANSITION_NAME, 1);
			
			for(var i : int = 0; i < _indicators.length; i ++)
			{
				_indicators[i].isSelected = (pageNumber == i + 1);
			}
		}
		
		public function enterAnimation():void
		{
			var delay : Number = .03;
			
			for each(var page : LevelPage in _pages)
			{
				page.showLevelIcons(delay);
			}
			
			for each(var indicator : PageIndicator in _indicators)
			{
				indicator.show(delay);
				delay += .1;
			}
			
		}
	}
}