package Graphs.ui.SelectLevelMode.component
{
	import Graphs.ui.GameMode.levels.LevelGenerator;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class LevelPage extends Sprite
	{
		private var _column:int;
		private var _row:int;
		private var _pageHeight:Number;
		private var _page:uint;
		private var _levelIcons:Vector.<LevelIcon>;;
		private var _controller:LevelPageViewController;
		
		
		public function LevelPage(pageController : LevelPageViewController, page : uint, row : int, column : int, pageHeight : Number)
		{
			super();
			
			_controller = pageController;
			_page = page;
			_row = row;
			_column = column;
			_pageHeight = pageHeight;
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e : Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			initBg();
			initLevelIcons();
			
			LevelGenerator.$.addEventListener(Const.UNLOCKED_NUMBER_CHANGED, unlockedNumberChanged);
		}
		
		private function unlockedNumberChanged(e : Event):void
		{
			var unlockedNumber : int = _controller.unlockedLevel;
			
			for each
			 (var icon : LevelIcon in _levelIcons)
			{
				if(icon.levelIndex > unlockedNumber)
				{
					icon.lock();
				}
				else
				{
					icon.unlock();
				}
			}
		}
		
		private function initBg():void
		{
			var quad : Quad = new Quad(Const.SCREEN_WIDTH, _pageHeight, 0);
			quad.alpha = 0;
			addChild(quad);
		}
		
		public function showLevelIcons(delayStep : Number) : void
		{
			var delay : Number = 0;
			for each(var icon : LevelIcon in _levelIcons)
			{
				delay = Math.random() * .3;
				icon.show(delay);
//				delay += delayStep;
			}
		}
		
		private function initLevelIcons():void
		{
			var pageCapacity : int = _row * _column;
			var levelIndex : int = 0;
			var levelIcon : LevelIcon;
			var isLocked : Boolean;
			
			var verticalStep : Number = (_pageHeight ) / (_row);
			var horizontalStep : Number = this.width / (_column + 1);
			
			var xCoord : Number;
			var yCoord : Number;
			
			var iconSize : Number = Const.SCREEN_WIDTH * .18;
			var padding : Number = Const.SCREEN_WIDTH * .15;
			
			_levelIcons = new Vector.<LevelIcon>;
			
			for(var i :int = 0; i < _row; i++)
			{
				yCoord = (i) * verticalStep + padding;
				
				for(var j : int = 0; j < _column; j++)
				{
					xCoord = (j + 1) * horizontalStep;
					
					levelIndex = i * _column + j + 1 + pageCapacity * (_page - 1);
					if(levelIndex > _controller.levelLength)
					{
						return;
					}
					
					isLocked = (levelIndex > _controller.unlockedLevel);
					
					levelIcon = new LevelIcon(_controller,levelIndex, isLocked, iconSize, xCoord, yCoord);
					addChild(levelIcon);
					_levelIcons.push(levelIcon);
				}
			}
		}
	}
}