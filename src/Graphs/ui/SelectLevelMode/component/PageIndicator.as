package Graphs.ui.SelectLevelMode.component
{
	import Graphs.Main;
	import Graphs.ui.components.BaseComponent;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class PageIndicator extends BaseComponent
	{
		private static const SELECTED_COLOR : int = 0x111111;
		private static const NOT_SELECTED_COLOR : int = 0xaaaaaa;
		
		private var _pageNumber:int;
		private var _size:Number;
		private var _controller:IPageController;
		private var _isSelected:Boolean;
		private var _icon:Image;
		
		public function PageIndicator(controller : IPageController, pageNumber : int, size : Number)
		{
			super();
			
			_controller = controller;
			_pageNumber = pageNumber;
			_size = size;
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e : Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			_icon = new Image(Main.$.assets.getTexture(Const.TEXTURE_DOT));
			addChild(_icon);
			_icon.width = _size;
			_icon.height = _size;
			_icon.color = NOT_SELECTED_COLOR;
			
			addEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		public function set isSelected(value : Boolean) : void
		{
			_isSelected = value;
			if(_isSelected)
			{
				_icon.color = SELECTED_COLOR;
			}
			else
			{
				_icon.color = NOT_SELECTED_COLOR;
			}
		}
		
		public function get isSelected() : Boolean
		{
			return _isSelected;	
		}
		
		private function touchHandler(e : TouchEvent):void
		{
			var touch : Touch = e.getTouch(e.currentTarget as DisplayObject, TouchPhase.BEGAN)
			
			if(touch && !_isSelected)
			{
				_controller.changePage(_pageNumber);
			}
		}
	}
}