package Graphs.ui.SelectLevelMode.component
{
	import Graphs.Main;
	import Graphs.ui.components.BaseComponent;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	
	public class LevelIcon extends BaseComponent
	{
		private var _levelIndex:int;
		private var _size:Number;
		private var _yCoord:Number;
		private var _xCoord:Number;
		private var _controller:LevelPageViewController;
		private var _levelSelected:Boolean;
		private var _isLocked:Boolean;
		
		private static const UNLOCKED_COLOR : uint = 0x111111; 
		private static const LOCKED_COLOR : uint = 0x999999; 

		private var _icon:Image;
		private var _touchBegan:Boolean;
		
		public function LevelIcon(controller : LevelPageViewController, levelIndex : int, isLocked : Boolean, size : Number, xCoord : Number, yCoord : Number)
		{
			super();
			
			_controller = controller;
			_levelIndex = levelIndex;
			_size = size;
			_xCoord = xCoord;
			_yCoord = yCoord;
			_isLocked = isLocked;
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		public function get levelIndex():int
		{
			return _levelIndex;
		}

		public function unlock() : void
		{
			if(_isLocked)
			{
				_isLocked = false;
				_icon.color = UNLOCKED_COLOR;
			}
		}
		
		public function lock() : void
		{
			if(!_isLocked)
			{
				_isLocked = true;
				_icon.color = LOCKED_COLOR;
			}
		}
		
		private function initialize(e : Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			var color : uint;
			if(_isLocked)
			{
				color = LOCKED_COLOR;
			}
			else
			{
				color = UNLOCKED_COLOR
			}
			
			_icon = new Image(Main.$.assets.getTexture(Const.TEXTURE_DOT));
			_icon.alignPivot();
			addChild(_icon);
			_icon.color = color;
			_icon.width = _size;
			_icon.height = _size;
			
			var levelText : TextField = new TextField(_size, _size, _levelIndex.toString(), "Verdana", _size * .6 , Const.COLOR_BG);
			levelText.alignPivot();
			addChild(levelText);
			
			this.x = _xCoord;
			this.y = _yCoord;
			
			addEventListener(TouchEvent.TOUCH, touchHandler);
			
			_controller.addEventListener(Const.RESET, resetLevelIcon);
			_controller.addEventListener(Const.LEVEL_SELECTED, onLevelSelected);
		}
		
		private function resetLevelIcon(e : Event):void
		{
			_levelSelected = false;
		}
		
		private function onLevelSelected(e : Event):void
		{
			_levelSelected = true;
		}
		
		private function touchHandler(e : TouchEvent):void
		{
			var touch : Touch = e.getTouch(this);
			
			if(touch && !_isLocked && !_levelSelected)
			{
				if(touch.phase == TouchPhase.BEGAN)
				{
					_tween.tween(this, .1, Const.TRANSITION_NAME, .9, .8, 0);
					_touchBegan = true;
				}
				if(touch.phase == TouchPhase.MOVED && _touchBegan && isTouchedOut(touch.globalX, touch.globalY))
				{
					_tween.tween(this, .1, Const.TRANSITION_NAME, 1,1,0);
					_touchBegan = false;
				}
				if(touch.phase == TouchPhase.ENDED && _touchBegan)
				{
					_tween.tween(this, .1, Const.TRANSITION_NAME, 1,1,0, touchAnimationComplete);
				}
			}
		}
		
		private function isTouchedOut(xTouch : Number, yTouch : Number):Boolean
		{
			var xProection : Number = xTouch - this.x;
			var yProection : Number = yTouch - this.y; 
			var distance : Number = Math.sqrt(Math.pow(xProection, 2) + Math.pow(yProection, 2));
			
			if(distance > _size / 2)
			{
				return true;
			}
			return false;
		}
		
		private function touchAnimationComplete() : void
		{
			_controller.dispatchEventWith(Const.LEVEL_SELECTED, false, _levelIndex - 1);
		}
	}
}