package Graphs.ui.MenuMode
{
	import Graphs.Main;
	import Graphs.ui.components.BaseMode;
	import Graphs.ui.components.ButtonSpecial;
	import Graphs.ui.components.IMode;
	import Graphs.utils.LayoutUtils;
	
	import starling.animation.Transitions;
	import starling.display.Image;
	import starling.events.Event;
	
	public class StartMenuModeView extends BaseMode implements IMode
	{

		private var _titleImage:Image;
		private var _leaderboardButton:ButtonSpecial;
		private var _playButton:ButtonSpecial;
		private var _rateButton:ButtonSpecial;
		
		
				
		public function StartMenuModeView( )
		{	
			super();
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		protected function initialize(e : Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			initBg();
			initTitle();
			initButtons();
		}		
		
		private function startOpenAnimation():void
		{
			var animationTime : Number = .4;
			_tween.moveTo(_titleImage, animationTime, -1,   Const.SCREEN_SIZE * .05, 0.3, Const.TRANSITION_NAME, 1);
		}
		
		private function initTitle():void
		{
			_titleImage = new Image(Main.$.assets.getTexture(Const.TEXTURE_TITLE));
			_titleImage.width =  Const.SCREEN_WIDTH * .6;
			_titleImage.height =   Const.SCREEN_SIZE * .1;
			addChild(_titleImage);
			LayoutUtils.$.centralAlign(_titleImage);
		}
		
		private function initButtons():void
		{
			var animationTime : Number = .4;
			var padding : int = Const.SCREEN_HEIGHT * 0.06;
			
			_playButton = new ButtonSpecial(Main.$.assets.getTexture(Const.TEXTURE_PLAY));
			addChild(_playButton);
			_playButton.initTouchHandler(playButtonHandler);
			_playButton.setSize(Const.SCREEN_SIZE * .2, Const.SCREEN_SIZE * .2);
			LayoutUtils.$.centralAlign(_playButton, 0, -Const.SCREEN_SIZE * 0.05);
			
			_leaderboardButton = new ButtonSpecial(Main.$.assets.getTexture(Const.TEXTURE_LEADERBOARD));
			addChild(_leaderboardButton);
			_leaderboardButton.initTouchHandler(leaderboardButtonHandler);
			_leaderboardButton.setSize(Const.SCREEN_SIZE * .1, Const.SCREEN_SIZE * .1);
			LayoutUtils.$.left(_leaderboardButton, padding);
			
			_rateButton = new ButtonSpecial(Main.$.assets.getTexture(Const.TEXTURE_RATE));
			addChild(_rateButton);
			_rateButton.initTouchHandler(rateButtonHandler);
			_rateButton.setSize(Const.SCREEN_SIZE * .1, Const.SCREEN_SIZE * .1);
			LayoutUtils.$.right(_rateButton, padding);
		}
		
		
		
		private function rateButtonHandler():void
		{
		}
		
		private function leaderboardButtonHandler():void
		{
		}
		
		private function playButtonHandler():void
		{
			Main.$.openSelectLevel(Const.DIRECTION_LEFT);
		}
		
		override public function enterAnimation( direction : String) : void
		{
			Main.mainContainer.addChild(this);
			super.enterAnimation(direction);
			
			hideButtons();
			tweenButtons();
		}
		
		
		override public function exitAnimation(direction : String) : void
		{
			super.exitAnimation(direction);
		}
		
		private function tweenButtons() : void
		{
			// animate buttons
			var animationTime : Number = .4;
			var transition : String = Transitions.EASE_IN_OUT;
			
			_tween.moveTo(_titleImage, animationTime, -1,   Const.SCREEN_SIZE * .05, 0.3, transition);
			_tween.moveTo(_playButton, animationTime, -1,   Const.SCREEN_HEIGHT / 2 - _playButton.height /2, .6, transition );
			_tween.moveTo(_leaderboardButton, animationTime, -1,   Const.SCREEN_HEIGHT * 0.8, .9, transition );
			_tween.moveTo(_rateButton, animationTime, -1,   Const.SCREEN_HEIGHT * 0.8, 1.1, transition );
		}
		
		private function hideButtons() : void
		{
			// hide
			_titleImage.alpha = 0;
			_playButton.alpha = 0;
			_leaderboardButton.alpha = 0;
			_rateButton.alpha = 0;
			
			// set the buttons top of the screen
			LayoutUtils.$.top(_titleImage, - _titleImage.height);
			LayoutUtils.$.top(_leaderboardButton, - _leaderboardButton.height);
			LayoutUtils.$.top(_playButton, - _playButton.height);
			LayoutUtils.$.top(_rateButton, - _rateButton.height);
		}
	}
}