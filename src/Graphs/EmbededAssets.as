package Graphs
{
	public class EmbededAssets
	{
		// Texture
		[Embed(source="/assets/texture/graphs.png")]
		public static const graphs:Class;
		
		// XML
		[Embed(source="/assets/texture/graphsData.xml", mimeType="application/octet-stream")]
		public static const graphsData:Class;
		
		// JSON
		[Embed(source="/assets/texture/levels.txt", mimeType="application/octet-stream")]
		public static const levels:Class;
	}
}