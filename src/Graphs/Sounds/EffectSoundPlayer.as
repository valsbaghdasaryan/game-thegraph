package Graphs.Sounds
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;

	public class EffectSoundPlayer implements ISoundPlayer
	{
		private var _sounds:Dictionary;
		
		
		
		public function EffectSoundPlayer()
		{
			_sounds = Dictionary;
			
			initSound(SoundsName.EFFECT_SWIPE);
			
			
			var stream : URLRequest = new URLRequest();
			
			var soundChannel : SoundChannel;
			var sound : Sound = new Sound(stream);
			sound.play(0,Number.MAX_VALUE);
		}
		
		
		private function initSound(name : String) : void
		{
			
		}
		
		public function play(soundName:String):void
		{
			
		}
	}
}