package Graphs.Sounds
{
	import starling.events.EventDispatcher;
	
	public class SoundsController extends EventDispatcher
	{
		private static var _instance:SoundsController;
		
		public static function get $():SoundsController
		{
			if(!_instance)
			{
				_instance = new SoundsController();
			}
			return _instance;
		}
		
		public function SoundsController()
		{
			if(_instance) {
				throw new Error("another SoundsController exists");
			}
		}
	}
}