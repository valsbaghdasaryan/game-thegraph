package Graphs
{
	
	import Graphs.ui.GameMode.GameModeView;
	import Graphs.ui.GameMode.levels.LevelGenerator;
	import Graphs.ui.MenuMode.StartMenuModeView;
	import Graphs.ui.SelectLevelMode.SelectLevelMode;
	import Graphs.ui.components.IMode;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	public class Main extends Sprite
	{
		private static var _instance:Main;
		
		public static var mainContainer : Sprite;
		public static var levelsLoaded : Boolean = false;
		
		private var _menuMode:StartMenuModeView;
		private var _levelMode:SelectLevelMode;
		private var _gameMode:GameModeView;
		private var _activeMode : IMode;
		private var _assets:AssetManager;
		
		
		public function Main()
		{
			if(_instance) {
				throw new Error("Main class is a singleton");
			}
			else
			{
				_instance = this;
				
				// set frame rate 60fps			
				Starling.current.nativeStage.frameRate = 60;
				
				//start loading
				_assets = new AssetManager();
				_assets.verbose = true;
				_assets.enqueue(EmbededAssets);
				_assets.loadQueue(initialize);
			}
		}
		
		
		
		public function get assets():AssetManager
		{
			return _assets;
		}

		public static function get $():Main
		{
			if(!_instance)
			{
				_instance = new Main();
			}
			return _instance;
		}
		
		private function levelsLoadedHandler(e : Event):void
		{
			levelsLoaded = true;
		}
		
		private function initialize(ratio:Number):void
		{
			//init screensize
			if(!_assets.isLoading)
			{
				Const.SCREEN_WIDTH = stage.stageWidth;
				Const.SCREEN_HEIGHT = stage.stageHeight;
				Const.SCREEN_SIZE = Math.sqrt(Math.pow(Const.SCREEN_WIDTH, 2) + Math.pow(Const.SCREEN_HEIGHT, 2));
				Const.GAME_HEIGHT = stage.stageHeight * 0.85;
				
				stage.color = Const.COLOR_BG;
				LevelGenerator.$.parseLevels();
				initModes();
				
				openMenu();
			}
		}
		
		private function initModes():void
		{
			mainContainer = new Sprite();
			addChild(mainContainer);
			
			_menuMode = new StartMenuModeView();
			_gameMode = new GameModeView();
			_levelMode = new SelectLevelMode();
		}
		
		public function startGame() : void
		{
			open(_gameMode, Const.DIRECTION_DOWN);
		}
		
		public function openMenu() : void
		{
			open(_menuMode, Const.DIRECTION_RIGHT);
		}
		
		public function openSelectLevel(dir : String) : void
		{
			open(_levelMode, dir);
		}
		
		private function open(mode : IMode, direction : String) : void
		{
			if(_activeMode)
			{
				_activeMode.exitAnimation(direction);
				mode.enterAnimation(direction);
			}
			else
			{
				mode.enterAnimation("");
			}
			
			_activeMode = mode;
		}
		
	}
}