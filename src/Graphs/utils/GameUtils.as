package Graphs.utils
{
	import Graphs.ui.GameMode.components.Dot;
	import Graphs.ui.GameMode.components.Line;
	
	import starling.display.Quad;
	import starling.display.Sprite;

	public class GameUtils
	{
		private static var _instance:GameUtils;
		
		public static function get $():GameUtils
		{
			if(!_instance)
			{
				_instance = new GameUtils();
			}
			return _instance;
		}
		
		public function GameUtils()
		{
			if(_instance) {
				throw new Error("another GameUtils exists");
			}
		}
		
		public function createContainer(contWidth : Number, contHeight: Number, alpha : Number = 1, color : Number = Const.COLOR_GREY_LIGHT) : Sprite
		{
			var container : Sprite = new Sprite;
			var quad : Quad = new Quad(contWidth, contHeight, color);
			quad.alpha = alpha;
			container.addChild(quad);
			
			return container;
		}
		
		public function shuffleLines( lines : Vector.<Line>) : void
		{
			var line : Line;
			var randomIndex : int;
			
			for(var i : int = 0; i < lines.length ; i ++)
			{
				randomIndex = Math.random() * lines.length; 
				line = lines[randomIndex];
				lines[randomIndex] = lines[i];
				lines[i] = line;
			}
			
			for(i = 0; i < lines.length ; i ++)
			{
				if(lines[i].color == Const.COLOR_GREY_DARK)
				{
					line = lines[lines.length - 1];
					lines[lines.length - 1] = lines[i];
					lines[i] = line;
				}
			}
		}
		
		
		public function shuffle(dots : Vector.<Dot>) : Vector.<Dot>
		{
			var dot : Dot;
			var randomIndex : int;
			
			for(var i : int = 0; i < dots.length ; i ++)
			{
				randomIndex = Math.random() * dots.length; 
				dot = dots[randomIndex];
				dots[randomIndex] = dots[i];
				dots[i] = dot;
			}
			for(i = 0; i < dots.length ; i ++)
			{
				randomIndex = Math.random() * dots.length; 
				dot = dots[randomIndex];
				dots[randomIndex] = dots[i];
				dots[i] = dot;
			}
			
			return dots;
		}
		
		public function getRandomInteger(from:int, to:int):int
		{
			var x : Number = to - from;
			var number : int = x * Math.random() + from; 
			return number;
		}
		
		public function getRandomNumber(from:Number, to:Number):Number
		{
			var number : Number = Math.random() * (to - from) + from; 
			return number;
		}
	}
}