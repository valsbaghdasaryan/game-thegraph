package Graphs.utils
{
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObject;

	public class LayoutUtils
	{
		private static var _instance:LayoutUtils;
		
		public static function get $():LayoutUtils
		{
			if(!_instance)
			{
				_instance = new LayoutUtils();
			}
			return _instance;
		}
		
		public function LayoutUtils()
		{
			if(_instance) {
				throw new Error("another LayoutUtils exists");
			}
		}
		
		public function centralAlign(target : DisplayObject, xAlign : Number  = 0, yAlign : Number = 0) : void
		{
			
			var parent : DisplayObject = target.parent;
			target.x = parent.width / 2 - target.width / 2 + xAlign;
			target.y = parent.height / 2 - target.height / 2 + yAlign;
		}
		
		public function bottom(target : DisplayObject,  padding : Number ,bottomDisplayObject : DisplayObject = null) : void
		{
			var bottomCoord : int;
			if(bottomDisplayObject)
			{
				bottomCoord = bottomDisplayObject.y
			}
			else
			{
				bottomCoord = target.parent.height;
				
			}
			target.y = bottomCoord - padding - target.height;
			
		}
		
		public function right(target : DisplayObject,  padding : Number ,rightDisplayObject : DisplayObject = null) : void
		{
			var rightCoord : int;
			if(rightDisplayObject)
			{
				rightCoord = rightDisplayObject.x
			}
			else
			{
				rightCoord = target.parent.width;
				
			}
			target.x = rightCoord - padding - target.width;
			
		}


		public function left(target : DisplayObject,  padding : Number ,leftDisplayObject : DisplayObject = null) : void
		{
			if(leftDisplayObject)
			{
				target.x = leftDisplayObject.x + leftDisplayObject.width + padding;
			}
			else
			{
				target.x = padding
			}
		}
		
		public function top(target : DisplayObject,  padding : Number ,topDisplayObject : DisplayObject = null) : void
		{
			if(topDisplayObject)
			{
				target.y = topDisplayObject.y + topDisplayObject.height + padding;
			}
			else
			{
				target.y = padding
				
			}
		}
			
	}
}